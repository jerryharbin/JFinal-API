# 配置 undertow，更多配置见官方文档：
# https://www.jfinal.com/doc/1-4

undertow.devMode=true
undertow.host=127.0.0.1
undertow.port=8080

# 接口请求实时报告功能配置
com.yunfinal.api.service.doc.ApiReportSocket=true
