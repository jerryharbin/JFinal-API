package com.yunfinal.api.service.plugin;

import com.jfinal.kit.Prop;
import com.jfinal.plugin.IPlugin;
import com.jfinal.wxaapp.WxaConfig;
import com.jfinal.wxaapp.WxaConfigKit;
import com.yunfinal.api.service.JFinalApiConfig;

/**
 * 方便后续可扩展
 */
public class WxaConfigPlugin implements IPlugin {

    @Override
    public boolean start() {
        Prop p = JFinalApiConfig.getProp();
        WxaConfig wc = new WxaConfig();
        wc.setAppId(p.get("wxa.appId"));
        wc.setAppSecret(p.get("wxa.appSecret"));
        WxaConfigKit.setWxaConfig(wc);
        return true;
    }

    @Override
    public boolean stop() {
        return true;
    }
}
