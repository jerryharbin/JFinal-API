package com.yunfinal.api.service.db.dao;

import com.jfinal.plugin.activerecord.SqlPara;

public class CondKvKit {
    public static final CondKvKit ME = new CondKvKit();

    public boolean isArray(Object value) {
        return null == value ? false : value.getClass().isArray();
    }

    /**
     * 添加 一个数组 匹配 占位符的 参数
     *
     * @param sqlPara
     * @param value
     * @return 字符串 “”
     */
    public String addArray(SqlPara sqlPara, Object[] value) {
        for (Object v : value) {
            sqlPara.addPara(v);
        }
        return "";
    }
}
