package com.yunfinal.api.service;

/***
 * 接口异常 编码
 *
 * @author 杜福忠 2018-08-23 14:35:06
 *
 */
public class ApiException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public String code;
    public String msg;
    public Object data;

    /**
     * 自定义异常编码和错误信息
     */
    public ApiException(String code, String msg) {
        super(new StringBuilder(code).append(":").append(msg).toString());
        this.code = code;
        this.msg = msg;
    }

    /**
     * 自定义异常编码和错误信息和数据
     */
    public ApiException(String code, String msg, Object data) {
        this(code, msg);
        this.data = data;
    }

    @Override
    public String toString() {
        return "YunException [code=" + code + ", msg=" + msg + ", data=" + data
                + "]";
    }

}
