package com.yunfinal.api.service.doc;

import com.alibaba.fastjson.JSONObject;
import com.yunfinal.api.service.ApiHandleController;

public class ApiDocController extends ApiHandleController {

    @Override
    @ApiDoc("系统API文档数据")
    public Object index(JSONObject kv) {
        return ApiDocService.ME.all();
    }

    @ApiDoc("系统API文档详情")
    public Object details(JSONObject kv) {
        return ApiDocService.ME.details(kv);
    }

}
