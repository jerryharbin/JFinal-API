package com.yunfinal.api.demo;

import com.jfinal.config.Constants;
import com.jfinal.config.Routes;
import com.yunfinal.api.service.JFinalApiConfig;
import com.yunfinal.api.service.doc.ApiDocController;

/**
 * JFinal配置项
 */
public class DemoApiConfig extends JFinalApiConfig {
    public static void main(String[] args) {
        start(DemoApiConfig.class);
    }

    @Override
    public void configConstant(Constants me) {
        //super不能丢
        super.configConstant(me);
        // me.setDevMode(false);
    }

    @Override
    public void configRoute(Routes me) {
        super.configRoute(me);
        // 注册自己的 控制器API
        me.add("/", DemoIndex.class);
        me.add("/api", DemoApi.class);
        me.add("/mini/app/api/allActionKeys", ApiDocController.class);
    }
}
