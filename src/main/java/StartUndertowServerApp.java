import com.yunfinal.api.demo.DemoApiConfig;

/**
 * 启动项目，右键菜单》 Run
 */
public class StartUndertowServerApp {
    public static void main(String[] args) {
        DemoApiConfig.main(args);
    }
}
